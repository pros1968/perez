<div class="main-wrap bg-white" ng-init="check_single_packing_list();">
  <div class="save-info">
    @include('host_experiences.manage_experience.header', ['header_inverse' => true])
  </div>
  <div class="col-12 col-lg-7 main-wrap-info mt-lg-4">
    <h3>
      {{trans('experiences.manage.create_a_packing_list')}}
    </h3>
    <p>
      {{trans('experiences.manage.lets_guests_know_what_to_bring')}}
    </p>
    <h5>
      @{{host_experience.title}}
    </h5>
    <p>
      {{trans('experiences.manage.what_should_your_guests_bring')}}
    </p>
    <div class="clone_elem package-input" ng-repeat="(key,item) in host_experience_packing_lists">
      {!! Form::select('title_language[]', $languages, 'en', ['class' => 'form-control input-title', 'id' => 'input_status', 'placeholder' => 'Select','disabled']) !!}
      <div class="item_name_cls">
        <span class="icon icon2-cancel cursor" ng-click="remove_packing_list($index)"></span>
        <input type="hidden" name="packing_lists[][id]" id="packing_list_@{{$index}}_id" ng-model="host_experience_packing_lists[$index].id">
        <input type="text" class="input_new1 my-2" placeholder="{{trans('experiences.manage.enter_item_here')}}" name="packing_lists[][item]" id="packing_list_@{{$index}}_item" ng-model="item.item" ng-change="host_experience_packing_lists_changed(); need_packing_lists_change();">
      </div>
      <div class="package_length" ng-show="item.item.length">
        <h4>Translate</h4>
        <p ng-repeat="lang in item.translate" class="select package_language">
          <span ng-click="item.translate.splice($index, 1);host_experience_packing_lists_changed()" class="fa fa-trash"></span>
          <i class="icon-chevron-down"></i>
          <select name="lang_translations[@{{$index}}][locale]" class="form-control trans"  ng-model="lang.locale" ng-change="host_experience_packing_lists_changed()">
            <option value="" ng-if="lang.locale == ''">Select Language</option>
              @foreach($languages as $key => $value)
                {{$key}}
                <option value="{{$key}}" ng-if="(('{{$key}}' | checkKeyValueUsedInStack : 'locale': item.translate) || '{{$key}}' == lang.locale) && '{{$key}}' != 'en'">{{$value}}</option>
              @endforeach
          </select>

          <input type="text" class="input_new1 my-2" placeholder="Enter item here" name="packing_lists[][item]" id="packing_list_@{{$index}}_item" ng-model="lang.item" ng-change="host_experience_packing_lists_changed();" >
          <a href="javascript:void(0)" ng-show="$last && item.translate[item.translate.length-1].locale!=''&& item.translate[item.translate.length-1].item!='' && item.translate.length < {{count($languages)-1}}" class="mt-3 host-add-item"  ng-click="item.translate.push({'locale':'','item':''});host_experience_packing_lists_changed()">Add</a>
        </p>
        <a href="javascript:void(0)" ng-show="item.translate.length==0" class="mt-3 host-add-item"  ng-click="item.translate.push({'locale':'','item':''});host_experience_packing_lists_changed()">Add</a>
      </div>
    </div>
    <a href="javascript:void(0)" class="mt-3 host-add-item" ng-if="packing_list_can_add_more" ng-click="add_packing_list();"> 
      + {{trans('experiences.manage.add_an_item')}}
    </a>
    <div class="mt-4" id="need_packing_lists_part" test="@{{ host_experience_packing_lists }}" ng-show="host_experience_packing_lists.length == 0">
      <h5>
        {{trans('experiences.manage.do_guests_not_bringing_anything')}}
      </h5>
      <p>
        {{trans('experiences.manage.if_so_make_sure_you_provide_everything')}}
      </p>
      <div class="my-4">
        <label class="verify-check">
          <input type="checkbox" name="need_packing_lists" ng-model="host_experience.need_packing_lists" ng-true-value="'No'" ng-false-value="false" ng-checked="host_experience.need_packing_lists == 'No'"> 
          <span>
           {{trans('experiences.manage.my_guests_dont_need_to_bring_anything')}}
         </span>
         <p class="text-danger" ng-show="form_errors.need_packing_lists.length">
          @{{form_errors.need_packing_lists[0]}}
        </p>
      </label>
    </div>
  </div>
  <div class="mt-4">
    @include('host_experiences.manage_experience.control_buttons')
  </div>
</div>
</div>
<!--  main_bar end -->