  
  <script type="text/javascript">
  var translations_value = "{!! $column.'_translations' !!}";
  </script>
  @php
    $translations_var = $column.'_translations';
  @endphp
  <div class="panel"  ng-init="lang_translations = getLang('{{$translations_var}}'); remove_lang_translations =  [];">
    <div class="panel-body title-cls" ng-init="languages = {{json_encode($languages)}}">
      <div class="select tagline-down" ng-repeat="translation in lang_translations">
        <i class="icon-chevron-down"></i>
        <select name="lang_translations[@{{$index}}][locale]" class="form-control trans" id="input_title_language_@{{$index}}" ng-model="translation.locale" ng-change="is_update({{$translations_var}})">
          <option value="" ng-if="translation.locale == ''">Select Language</option>
            @foreach($languages as $key => $value)
              {{$key}}
              @if($column=='directions')
              <option value="{{$key}}" ng-if="(('{{$key}}' | checkKeyValueUsedInStack : 'locale': host_experience.host_experience_location['{{$translations_var}}']) || '{{$key}}' == translation.locale) && '{{$key}}' != 'en'">{{$value}}</option>
              @else
              <option value="{{$key}}" ng-if="(('{{$key}}' | checkKeyValueUsedInStack : 'locale': host_experience['{{$translations_var}}']) || '{{$key}}' == translation.locale) && '{{$key}}' != 'en'">{{$value}}</option>

              @endif
            @endforeach
        </select>
        <div class="form-group add_title">
          <div class="col-sm-8 sub__add_title">
            @if(isset($text_area))
              <textarea class="form-control" ng-change="is_update('{{$translations_var}}')" ng-model="translation.{{$column}}" placeholder="{{trans('messages.place_hold.'.$column)}}">
              </textarea>
            @else
              <input type="text" name="" class="form-control" ng-change="is_update('{{$translations_var}}')" ng-model="translation.{{$column}}" placeholder="{{trans('messages.place_hold.'.$column)}}">
            @endif
            @if($column!='directions')
            <p class="focus_show mt-2" ng-class="character_length_class(0, {{$max_len}}, translation.{{$column}},true)">


              <span ng-bind="character_length_validation(0  , {{$max_len}}, translation['{{$column}}'],true)">
              </span>
            </p>
            @endif
          </div>
          <div class="col-md-4 sub__add_title" ng-show="lang_translations.length">
            <button class="btn btn-danger pull-right remove_lang_btn" ng-click="removeLang('{{$translations_var}}',$index)">Remove
            </button>
          </div> 
        </div>
      </div>                     
    </div>
  </div>
  <div class="row mb-4 mt-2" ng-show="lang_translations.length < {{count($languages)-1}}">
    <div class="col-sm-12">
      <button type="button" class="btn pull-right add_translate_btn" ng-click="addLang('{{$translations_var}}','{{$column}}')" >
        <i class="fa fa-plus"></i> Add Translation
      </button>
    </div>
  </div>