   
  <div class="panel"  >
    <div class="panel-body title-cls" ng-init="languages = {{json_encode($languages)}}">
      <div class="select" ng-repeat="translation in host_experience.special_certifications_translations">
        <i class="icon-chevron-down"></i>
        <select name="lang_translations[@{{$index}}][locale]" class="form-control trans" id="input_title_language_@{{$index}}" ng-model="translation.locale" ng-change="is_update()">
          <option value="" ng-if="translation.locale == ''">Select Language</option>
            @foreach($languages as $key => $value)
              {{$key}}
              <option value="{{$key}}" ng-if="(('{{$key}}' | checkKeyValueUsedInStack : 'locale': host_experience.special_certifications_translations) || '{{$key}}' == translation.locale) && '{{$key}}' != 'en'">{{$value}}</option>
            @endforeach
        </select>
        <div class="form-group add_title">
          <div class="col-sm-8 sub__add_title">
              <textarea class="form-control" ng-model="translation.special_certifications" placeholder="{{trans('messages.place_hold.special_certifications')}}" ng-keyup="is_update();">
              </textarea>
            
          </div>
          <div class="col-md-4 sub__add_title" ng-show="host_experience.special_certifications_translations.length">
            <button class="btn btn-danger pull-right remove_lang_btn" ng-click="host_experience.special_certifications_translations.splice($index, 1);is_update()">Remove
            </button>
          </div> 
        </div>
      </div>                     
    </div>
  </div>
  <div class="row mb-4 mt-2" ng-show="host_experience.special_certifications_translations.length < {{count($languages)}}">
    <div class="col-sm-12">
      <button type="button" class="btn pull-right add_translate_btn" ng-click="host_experience.special_certifications_translations.push({locale:''});is_update()" >
        <i class="fa fa-plus"></i> Add Translation
      </button>
    </div>
  </div>
