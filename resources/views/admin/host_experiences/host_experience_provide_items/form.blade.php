<div class="form-group">
    <label for="input_status" class="col-sm-3 control-label">Language<em class="text-danger">*</em></label>
    <div class="col-sm-6">
      {!! Form::select('language_code',$language, 'en', ['class' => 'form-control', 'id' => 'input_status', 'placeholder' => 'Select','disabled'=>'disabled']) !!}
      <span class="text-danger">{{ $errors->first('language_code') }}</span>
    </div>
</div>

<div class="form-group status">
  	<label for="input_status" class="col-sm-3 control-label">Name<em class="text-danger">*</em></label>
  	<div class="col-sm-6">
    	{!! Form::text('name', @$host_experience_provide_item->name, ['class' => 'form-control', 'id' => 'input_name', 'placeholder' => 'Name']) !!}                    
  		<span class="text-danger">{{ $errors->first('name') }}</span>
  	</div>
</div>
<div class="form-group status">
    <label for="input_status" class="col-sm-3 control-label">Image<em class="text-danger">*</em></label>
    <div class="col-sm-6">
      {!! Form::file('image', ['class' => 'form-control', 'id' => 'input_image']) !!}      
      <span class="text-info">Note: Preferred image dimenstions are 16x19</span>
      <br>
      <span class="text-danger">{{ $errors->first('image') }}</span>
      @if(@$host_experience_provide_item->image)
        <br>
        <img src="{{@$host_experience_provide_item->image_url}}" style="width: 16px; height: 19px;">
      @endif
    </div>
</div>
<div class="form-group status">
  	<label for="input_status" class="col-sm-3 control-label">Status<em class="text-danger">*</em></label>
  	<div class="col-sm-6">
    	{!! Form::select('status', array('Active' => 'Active', 'Inactive' => 'Inactive'), @$host_experience_provide_item->status, ['class' => 'form-control', 'id' => 'input_status', 'placeholder' => 'Select']) !!}                    
  		<span class="text-danger">{{ $errors->first('status') }}</span>
  	</div>
</div>

                  <div class="panel" ng-init="translations = {{json_encode(old('translations') ?:$translations)}}; removed_translations =  []; errors = {{json_encode($errors->getMessages())}};">
                  {{-- <div class="panel-header">
                    <h4 class="box-title text-center">Translations</h4>
                  </div> --}}
                  <div class="panel-body">
                    <input type="hidden" name="removed_translations" ng-value="removed_translations.toString()">
                    <div class="row" ng-repeat="translation in translations">
                      <input type="hidden" name="translations[@{{$index}}][id]" value="@{{translation.id}}">
                      <div class="form-group">
                        <label for="input_language_@{{$index}}" class="col-sm-3 control-label">Language<em class="text-danger">*</em></label>
                        <div class="col-sm-6">
                          <select name="translations[@{{$index}}][locale]" class="form-control" id="input_language_@{{$index}}" ng-model="translation.locale" >
                            <option value="" ng-if="translation.locale == ''">Select Language</option>
                            @foreach($languages as $key => $value)
                              <option value="{{$key}}" ng-if="(('{{$key}}' | checkKeyValueUsedInStack : 'locale': translations) || '{{$key}}' == translation.locale) && '{{$key}}' != 'en'">{{$value}}</option>
                            @endforeach
                          </select>
                          <span class="text-danger ">@{{ errors['translations.'+$index+'.locale'][0] }}</span>
                        </div>
                        <div class="col-sm-3">
                          <button class="btn btn-danger pull-right" ng-click="translations.splice($index, 1); removed_translations.push(translation.id)">
                            {{-- <i class="fa fa-trash"></i> --}}
                            Remove
                          </button>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="input_name_@{{$index}}" class="col-sm-3 control-label">Name<em class="text-danger">*</em></label>
                        <div class="col-sm-6">
                          {!! Form::text('translations[@{{$index}}][name]', '@{{translation.name}}', ['class' => 'form-control', 'id' => 'input_name_@{{$index}}', 'placeholder' => 'Name']) !!}
                          <span class="text-danger">@{{ errors['translations.'+$index+'.name'][0] }}</span>
                        </div>
                      </div>

                      <legend ng-if="$index+1 < translations.length"></legend>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="row" ng-show="translations.length <  {{count($languages) - 1}}">
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-info pull-right" ng-click="translations.push({locale:''});" >
                          <i class="fa fa-plus"></i> Add Translation
                        </button>
                      </div>
                    </div>
                  </div>
                </div>