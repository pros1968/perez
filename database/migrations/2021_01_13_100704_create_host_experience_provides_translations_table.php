<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostExperienceProvidesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('host_experience_provides_translations');
        Schema::create('host_experience_provides_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_experience_provide_id')->unsigned();
            $table->foreign('host_experience_provide_id', 'provide_id_foreign')->references('id')->on('host_experience_provides');
            $table->string('name', 100);
            $table->text('additional_details');
            $table->string('locale', 10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_experience_provides_translations');
    }
}
