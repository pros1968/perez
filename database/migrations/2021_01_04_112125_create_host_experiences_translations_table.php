<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostExperiencesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_experiences_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_experience_id')->unsigned();
            $table->foreign('host_experience_id')->references('id')->on('host_experiences');
            $table->string('title', 100);
            $table->string('tagline', 100);
            $table->text('what_will_do');
            $table->text('where_will_be');
            $table->text('notes');
            $table->text('about_you');
            $table->text('special_certifications');
            $table->text('additional_requirements');
            $table->string('step', 100);
            $table->string('locale', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_experiences_translations');
    }
}
