<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostExperienceProvideItemsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('host_experience_provide_items_translations');
        Schema::create('host_experience_provide_items_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_experience_provide_item_id')->unsigned();
            $table->foreign('host_experience_provide_item_id', 'pitem_id_foreign')->references('id')->on('host_experience_provide_items');
            $table->string('name', 50);
            $table->string('locale', 10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_experience_provide_items_translations');
    }
}
