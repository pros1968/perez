<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostExperienceLocationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_experience_location_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_experience_location_id')->unsigned();
            $table->foreign('host_experience_location_id','location_id')->references('id')->on('host_experience_location')->onDelete('cascade');
            $table->text('directions');
            $table->string('locale', 10); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_experience_location_translations');
    }
}
