<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostExperienceCategoriesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_experience_categories_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_experience_categories_id')->unsigned();
            $table->foreign('host_experience_categories_id','category_id')->references('id')->on('host_experience_categories')->onDelete('cascade');
            $table->string('name', 50);
            $table->string('locale', 10); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_experience_categories_translations');
    }
}
