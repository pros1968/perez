<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostExperiencePackingListsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('host_experience_packing_lists_translations');
        Schema::create('host_experience_packing_lists_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('host_experience_packing_list_id')->unsigned();
            $table->foreign('host_experience_packing_list_id','packing_list_id_foreign')->references('id')->on('host_experience_packing_lists');
            $table->text('item');
            $table->string('locale', 10);            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_experience_packing_lists_translations');
    }
}
