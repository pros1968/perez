<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperienceCategoriesTranslation extends Model
{
    //
    protected $table = 'host_experience_categories_translations';

    public $timestamps = false;    
}
