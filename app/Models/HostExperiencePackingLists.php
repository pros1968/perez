<?php

/**
 * HostExperiencePackingLists Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    HostExperiencePackingLists
 * @author      Trioangle Product Team
 * @version     2.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperiencePackingLists extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use Translatable;

    public $translatedAttributes = ['item'];

    protected $translationForeignKey = 'host_experience_packing_list_id';

    protected $table = 'host_experience_packing_lists';

    public $timestamps = false;

     //Join with host_experience_packing_lists Translations table
    public function host_experience_packing_lists_translations() {
        return $this->hasMany('App\Models\HostExperiencePackingListsTranslations', 'host_experience_packing_list_id', 'id');

    }
     //Join with host_experience_packing_lists Translations table
    public function translate() {
        return $this->hasMany('App\Models\HostExperiencePackingListsTranslations', 'host_experience_packing_list_id', 'id');

    }
}
