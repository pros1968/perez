<?php

/**
 * Language Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Language
 * @author      Trioangle Product Team
 * @version     2.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'language';

    public $timestamps = false;

    public function scopeActive($query) {
        return $query->where('status', 'Active');
    }

    public function scopeTranslatable($query) {
        return $query->active()->where('is_translatable', '1');
    }

    public function get_already_used_count($locale)
    {
        $host_experience_trans = HostExperiencesTranslations::where('locale',@$locale)->count();
        $packing_lists_trans   = HostExperiencePackingListsTranslations::where('locale',@$locale)->count();
        $provides_trans        = HostExperienceProvidesTranslations::where('locale',@$locale)->count();
        $location_trans        = HostExperienceLocationTranslations::where('locale',@$locale)->count();

        $provide_items_trans   = HostExperienceProvideItemsTranslations::where('locale',@$locale)->count();
        $categories_trans      = HostExperienceCategoriesTranslation::where('locale',@$locale)->count();
        
        if($host_experience_trans > 0 || $packing_lists_trans > 0 || $provides_trans > 0  || $location_trans > 0  || $provide_items_trans > 0 || $categories_trans > 0 ){
            return true;
        }
        return false;
    } 
}
