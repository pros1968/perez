<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperienceLocationTranslations extends Model
{
    //
    protected $table = 'host_experience_location_translations';

    public $timestamps = false; 

    public function language() {
        return $this->belongsTo('App\Models\Language','locale','value');
    }   
}
