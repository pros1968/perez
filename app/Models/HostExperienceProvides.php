<?php

/**
 * HostExperienceProvides Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    HostExperienceProvides
 * @author      Trioangle Product Team
 * @version     2.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class HostExperienceProvides extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
     use Translatable;

    public $translatedAttributes = ['name','additional_details'];

    protected $translationForeignKey = 'host_experience_provide_id';

    protected $table = 'host_experience_provides';

    public $timestamps = false;

    public function newQuery($ordered = true)
    {
        $query = parent::newQuery();

        if (empty($ordered)) {
            return $query;
        }

        return $query->orderBy('host_experience_provide_item_id', 'asc');
    }

    public function provide_item()
    {
    	return $this->belongsTo('App\Models\HostExperienceProvideItems','host_experience_provide_item_id','id');
    }

     //Join with host_experience_provided_items Translations table
    public function host_experience_provided_items_translations() {
        return $this->hasMany('App\Models\HostExperienceProvidesTranslations', 'host_experience_provide_id', 'id');

    }
     //Join with host_experience_provided_items Translations table
    public function translate() {
        return $this->hasMany('App\Models\HostExperienceProvidesTranslations', 'host_experience_provide_id', 'id');

    }

}
