<?php

/**
 * HostExperienceProvideItems Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    HostExperienceProvideItems
 * @author      Trioangle Product Team
 * @version     2.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class HostExperienceProvideItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'host_experience_provide_items';

    public $timestamps = true;

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        $url = '';
        if($this->attributes['image'])
        {
            $photo_src=explode('.',$this->attributes['image']);
            if(count($photo_src)>1)
            {
                $url = url('images/host_experiences/provide_items/'.$this->attributes['image']);
            }
            else
            {
                $options['secure']=TRUE;
                $options['width']=16;
                $options['height']=19;
                $url =\Cloudder::show($this->attributes['image'],$options);
            }
        }
        return $url;
    }
    
    public function scopeActive($query)
    {
    	$query = $query->where('status', 'Active');
    	return $query;
    }

    public function getNameAttribute()
    {
        $step_name = 'name';
        return $this->get_translation_data($step_name);
    }

    public function get_translation_data($step_name){
        if( request()->segment(1) == 'admin' ||  (request()->segment(1) == 'host' && (request()->segment(2) == 'ajax_manage_experience' || request()->segment(2) == 'manage_experience'))) {

            return $this->attributes[$step_name];

        }
        $default_lang = Language::where('default_language',1)->first()->value;

        $lang = Language::whereValue((Session::get('language')) ? Session::get('language') : $default_lang)->first()->value;

        if($lang == 'en')
            return $this->attributes[$step_name];
        else {
            $step = @HostExperienceProvideItemsTranslations::where('host_experience_provide_item_id', $this->attributes['id'])->where('locale', $lang)->first()->$step_name;
            if($step)
                return $step;
            else
                return $this->attributes[$step_name];
        }        
    }    
}
