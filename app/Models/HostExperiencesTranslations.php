<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperiencesTranslations extends Model
{
    //
    protected $table = 'host_experiences_translations';
    
    public function language() {
        return $this->belongsTo('App\Models\Language','locale','value');
    }
}
