<?php

/**
 * HostExperienceGuestRequirements Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    HostExperienceGuestRequirements
 * @author      Trioangle Product Team
 * @version     2.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperienceGuestRequirements extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'host_experience_guest_requirements';

    public $timestamps = false;


    // Get special_certifications
    public function getSpecialCertificationsAttribute() {
        return $this->get_translation_data('special_certifications');
    }

    // Get additional_requirements
    public function getAdditionalRequirementsAttribute() {
        return $this->get_translation_data('additional_requirements');
    }



    public function get_translation_data($column){
        if( request()->segment(1) == 'admin' ||  (request()->segment(1) == 'host' && (request()->segment(2) == 'ajax_manage_experience' || request()->segment(2) == 'manage_experience'))) {

        	return $this->attributes[$column];

        }
        $default_lang = Language::where('default_language',1)->first()->value;

        $lang = Language::whereValue((session()->get('language')) ? session()->get('language') : $default_lang)->first()->value;

        if($lang == 'en')
            return $this->attributes[$column];
        else {
            $step = @HostExperiencesTranslations::where('host_experience_id', $this->attributes['host_experience_id'])->where('locale', $lang)->where($column,'!=', '')->first()->$column;
            if($step)
                return $step;
            else
                return $this->attributes[$column];
        }        
    }
}
