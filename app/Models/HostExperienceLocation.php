<?php

/**
 * HostExperienceLocation Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    HostExperienceLocation
 * @author      Trioangle Product Team
 * @version     2.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperienceLocation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use Translatable;

    public $translatedAttributes = ['directions'];

    protected $table = 'host_experience_location';

    public $timestamps = false;

    // Get country_name by using country code in Country table
    public function getCountryNameAttribute()
    {
        return Country::where('short_name',$this->attributes['country'])->first()->long_name;
    }

    //Join with host_experience_location_translations table
    public function host_experience_location_translations() {
        return $this->hasMany('App\Models\HostExperienceLocationTranslations', 'host_experience_location_id', 'id');
    }    

     //get directions_translations 
    public function directions_translations() {
        return $this->hasMany('App\Models\HostExperienceLocationTranslations', 'host_experience_location_id', 'id')->where('directions','!=','');

    }
}
