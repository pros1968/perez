<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperienceProvidesTranslations extends Model
{
    //
    protected $table = 'host_experience_provides_translations';

    public $timestamps = false;

    public function language() {
        return $this->belongsTo('App\Models\Language','locale','value');
    }

}
