<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperienceProvideItemsTranslations extends Model
{
    //
    protected $table = 'host_experience_provide_items_translations';

    public $timestamps = false;    
}
