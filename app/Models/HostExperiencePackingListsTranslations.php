<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostExperiencePackingListsTranslations extends Model
{
    //
    protected $table = 'host_experience_packing_lists_translations';

    public $timestamps = false;

    public function language() {
        return $this->belongsTo('App\Models\Language','locale','value');
    }
}
